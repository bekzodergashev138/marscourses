# Addition
val1 = 2
val2 = 4

result = val1 + val2
print(result)

# Subtraction
val1 = 2
val2 = 3

result = val1 - val2
print(result)

# Multiplication
val1 = 2
val2 = 0.5

# using the multiplication operator
res = val1 * val2
print(res)

# Division
val1 = 3
val2 = 2

res = val1 / val2
print(res)

# Modulus, tt is used to find the remainder when first operand is divided by the second.
val1 = 3
val2 = 2

res = val1 % val2
print(res)

# Exponentiation
val1 = 2
val2 = 3

res = val1 ** val2
print(res)

# Floor division is a normal division operation except that it returns
# the largest possible integer.
# This integer is either less than or equal to the normal division result.
val1 = 3
val2 = 2

res = val1 // val2
print(res)

# Operator precedence
val1 = 1
val2 = 4
val3 = 9

result = val1 + val2 * val3
print(result)

result_a = (val1 + val2) * val3
print(result_a)

# Let students find out the reason of errors
val1 = 4
val2 = 0

result = val1 / val2
print(result)

val1 = 6
val2 = 9.3

# Explain about round
result = round(val1+val2)
print(result)






