"""
EcoBazar. There are five items in the store: Apples, Oranges, Tomatoes, Potatoes, Onions.
Each item has a price per kilogram. Create user input asking to choose an item and amount of kilos. Use tuple to
store input values. Print out the orders. Hints: items also stored in tuple in the same order, user
inputs a number of item.
"""

ITEMS = ("Apples", "Oranges", "Tomatoes", "Potatoes", "Onions")
PRICES = (20, 30, 15, 10, 5)


def bazar_items(idx, kilos):
    item = ITEMS[idx]
    price = PRICES[idx]
    total_price = kilos * price
    print(f"Your order is {item}, you pay {total_price}$")


if __name__ == '__main__':
    print(f"Welcome to EcoBazar, we have")
    print("1. Apples")
    print("2. Oranges")
    print("3. Tomatoes")
    print("4. Potatoes")
    print("5 Onions")

    item = input("Please select one of the them and enter its number.")
    item = int(item)
    if item not in (1, 2, 3, 4, 5):
        item = input("Please choose between 1 and 5")
    kilo = input(f"You chose {ITEMS[item]}, how many kilos you want? Enter numeric data!")
    bazar_items(item, float(kilo))


