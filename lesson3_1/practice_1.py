print(True)
print(False)

val_true = True
val_false = False

print(val_true, val_false)
print(type(val_true))

a_true_alias = True
# True = 5
# this cause an error cannot assign to True, Because True/False are keywords, you can’t assign a value to them.

# Python Booleans as Numbers, since boolean considered a numeric type we can use math expressions on it
# Basically, it's considered True=1, False=0
a_num = 1
b_num = 5
bool_result = a_num + True
print(bool_result)

# Let students guess the result
math_bool = False + True + False - True
print(math_bool)

# boolean types also convertable from str or int types
a_num = 1
bool_result = bool(a_num)
print(bool_result)

o_num = 0
bool_result = bool(o_num)
print(bool_result)

a_string = "True"
b_string = "False"

# Ask students why the result is True for "False"
print(bool(a_string), bool(b_string))

# Explain why empty string is False
empty_string = ""
print(bool(empty_string))

# We can convert to str
bool_value = False
string_bool = str(bool_value)
print(string_bool)

# We can convert to int
bool_value = False
int_bool = int(bool_value)
print(int_bool)
