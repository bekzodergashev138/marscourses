
"""
File catalogs. Program is intended to get information about files located in some specified folder.
The information should contain file size(in bytes), file extension, filename, if the file is txt formatted,
show the first 100 characters if its body.
Hints:
Use pathlib.Path package to get file size information.
Use os.listdir() to list all files in the directory.
Check for file extension, if it's txt open it and read first 10 characters.
Store a file information in dict type with according key values.
"""
import os
import shutil
from pathlib import Path

FILE_DATA = []


def get_file_size(file_path):
    return f"{Path(file_path).stat().st_size} bytes"


def get_file_format(file_name: str):
    return file_name[file_name.rindex('.'):]


def get_text_from_file(file):
    with open(file) as f:
        txt = " ".join(f.readlines())
    return f"{txt[:10]}..."


if __name__ == '__main__':
    dir_ = "sample_folder"
    files = os.listdir(dir_)
    for fl in files:
        file_path = f"{dir_}/{fl}"
        data = {"name": fl}
        if fl.endswith('.txt'):
            data['extension'] = 'txt'
            file_ext = get_text_from_file(file_path)
            data['extra_info'] = file_ext
        else:
            file_format = get_file_format(fl)
            data['extension'] = file_format
        file_size = get_file_size(file_path)
        data['size'] = file_size
        FILE_DATA.append(data)

    print(FILE_DATA)
