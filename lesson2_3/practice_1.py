# Ask students what this code does
a = 2
b = 3
c = 5
d = (- b+(b**2-4*a*c)**(1/2)) / (2*a)
e = (- b-(b**2-4*a*c)**(1/2)) / (2*a)

print(d, e)

# Documented option
"""Solve quadratic equation via the quadratic formula.

A quadratic equation has the following form:
ax**2 + bx + c = 0

There always two solutions to a quadratic equation: x_1 & x_2.
"""
x_1 = (- b+(b**2-4*a*c)**(1/2)) / (2*a)
x_2 = (- b-(b**2-4*a*c)**(1/2)) / (2*a)

print(x_1, x_2)

# Ask students what's wrong here
x = "Otabek"
y = 16
z = 10
t = "High School"
print(f"{x}, age {y}, studying in {z}s clas in {t}")

# One way
x = "Otabek"    # name
y = 16  # age
z = 10  # clas
t = "High School"   # study place
print(f"{x}, age {y}, studying in {z}s clas in {t}")

# Recommended way
name = "Otabek"
age = 16
clas_number = 10
school_type = "High School"
print(f"{name}, age {age}, studying in {clas_number}s clas in {school_type}")
