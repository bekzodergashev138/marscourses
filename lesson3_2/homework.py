
"""
Lie To Me. Create a program with 20 input questions, answer can be True or False,
as results it should sum all your answers and print out some score you answered right.
Hint: 0 and 1 can be converted as False and True
"""

print("Hello stranger, you are standing here to get all my gold treasures")
print("But you must pass my Quiz, If you collect the best scores the treasures yours, get ready!")
print("I accept only 0 or 1: 0 is NO, 1 is YES")

score = 0
right_answer = 0
answer_1 = input("The capital of USA is New York, it's YES or NO?: ")
if bool(answer_1):
    score = score - 1
else:
    right_answer = right_answer + 1
    score = score + 5
    
print(f"Your score is: {score}")
print(f"Your answered correctly to {right_answer} questions")